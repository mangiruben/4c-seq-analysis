#!/bin/bash

#
#SBATCH  --job-name=4_analyisis for read one
#SBATCH --output=read1_analysis.txt
#
#SBATCH --nodes=1
#SBATCH --time=0-3:00:00
#SBATCH --partition brc
#SBATCH --mem-per-cpu=64GB
#SBATCH --mail-type=END, FAIL
#SBATCH --mail-user=mangiruben@gmail.com
cd   ~/4C_analysis/read1/

module load utilities/use.dev
module load apps/bowtie/1.2.3
module load apps/bedtools2/2.29.0

perl ~/Scripts/4C_pipe.pl -f ~/fqfiles_r1 -c ~/Scripts/primers_r1.txt  -i ~/Ref_genomes/chr6_index/chr6.fa -n chr6_hg38_NlaIII-DpnII -g ~/Ref_genomes/chr6.fa -e ~/Scripts/enzyme_r1.txt







exit 0
