#!/bin/bash

#
#SBATCH  --job-name=4_analyisis for read one
#SBATCH --output=read1_analysis.txt
#
#SBATCH --nodes=1
#SBATCH --time=0-3:00:00
#SBATCH --partition brc
#SBATCH --mem-per-cpu=64GB
#SBATCH --mail-type=END, FAIL
#SBATCH --mail-user=mangiruben@gmail.com
cd   ~/4C_analysis/fqfiles_r2/

module load utilities/use.dev


python ~/Scripts/ -f ~/fqfiles_r2 -c GACGCCGGGGCAGGCTGATC -trim 30 -prefix H460_30bps_r2






exit 0
