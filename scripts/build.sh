#!/bin/bash

#
#SBATCH  --job-name=4_analyisis for read one
#SBATCH --output=read1_analysis.txt
#
#SBATCH --nodes=1
#SBATCH --time=0-3:00:00
#SBATCH --partition brc
#SBATCH --mem-per-cpu=64GB
#SBATCH --mail-type=END, FAIL
#SBATCH --mail-user=mangiruben@gmail.com


module load utilities/use.dev
module load apps/bowtie/1.2.3
module load apps/bedtools2/2.29.0

Bowtie –build ~/Ref_genomes/chr6.fa ~/Ref_genomes/chr6_index/chr6






exit 0
