#!/bin/bash

#
#SBATCH  --job-name=4_analyisis for read one
#SBATCH --output=read1_analysis.txt
#
#SBATCH --nodes=1
#SBATCH --time=0-3:00:00
#SBATCH --partition brc
#SBATCH --mem-per-cpu=64GB
#SBATCH --mail-type=END, FAIL
#SBATCH --mail-user=mangiruben@gmail.com
cd   ~/fqfiles_r2

module load utilities/use.dev

python ~/Scripts/demultiplex.py -f ~/raw/ -c ~/Scripts/primers_r2.txt  -c  ~/fqfiles_r2



exit 0
