**Regulation of _BRACHYURY_ (_TBXT)_ expression in cancers**
**Thesis 2021**


The repository has a collection of scripts codes and files used for to produce figures and further analysis of _TBXT_ 4C data in cancers for the above thesis. Detailed steps are on this [4C_ analysis workflow](https://gitlab.com/mangiruben/4c-seq-analysis/-/blob/master/accessories/4C_analysis_workflow.pdf)

The 4C template was prepared using NlaIII and DpnII restriction enzymes and  primers in 4C primers.txt file were used to prepare the template.The iF and iR primers in the 4C primers.txt file also specify the bait, this gives cordinates of the viewpoint for hg38 reference assembly.

Raw data was demultiplexed and trimmed to 30bps. I downloaded in fasta format the hg38 reference genome assembly for chromosome 6 from Ensembl [chrom 6.fa](https://www.ensembl.org/Homo_sapiens/Info/Index) and build genome index using bowtie in the High Perfomance Computing (HPC) environment. The trimmed reads were mapped to the indexed genome using 4C.pl pipeline on SLURM submitted job at the HPC. Further instructions on mapping are available at 4C.pl.

See the smoothened [4C profiles](https://genome.ucsc.edu/s/mangiruben/TBXT%204C-seq%20in%20cancers)

**Figures 3.7 and 3.8:**
The reads orientation alignment comparison was done by ploting the percentage of reads mapped in either orientation with information available in the bowtie.log. Smooth data  for H460_r1 and H460_r2 was uploaded in genome browsers to check profile peak distributions and enrichment between orientations at the viewpoint chromosome. 


**Figure 3.9:**
I Joined fragments from frag_counts files after mapping of the replicates to one  BED file and ran scatter_plots.R to check consistency between replicates. Further instructions available at scatter_plots.R.


**Figures 3.10 to 3.18:**
These plots were generaed after running interact.R to check interaction calling on fragscounts files following 4C.pl and then check plots if they are fine. Significant interactions are retained using True_interactions.R . Close interacting are merged from True_interactions. R results. 

**Figures 3.19 to 3.22:**
The Spider_plot. R draws SPlines from the view point to the marged regions. The Splines were overlayed and stacked to the merged signficant interactions in inkscape and the regulation landscape is visualised. Further instructions are available in Spider_plot.R

